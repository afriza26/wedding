@extends("frontend.layouts.app")

@section("title")
    Contact Us - United Square
@endsection
@section('js')
   <script src="{{url('https://www.google.com/recaptcha/api.js')}}"></script>

 @endsection
@section("content")


  <div class="breadcrumb">
              <div class="container">
                <ul class="breadcrumb__list">
                  <li class="breadcrumb__item"><a class="breadcrumb__link" href="{{url('/')}}" title="Home"><i class="ico ico-home"></i></a>
                  </li>
                  <li class="breadcrumb__item">Contact Us</li>
                </ul>
              </div>
            </div>
        <!-- Banner HTML-->
        <div class="banner">
        @if ($CmsBanner != ""  )
          @foreach ($CmsBanner as $banner)
              <div class="container"><img class="lazyload" alt="{{ $banner->name }}" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$banner->image)}}"></div>
          @endforeach
        @endif
        </div>
        <!-- Banner HTML-->
        <div class="container">
          <div class="row">
            <div class="col-xl-6">
              @if ($content_cms != ""  )
                  @foreach ($content_cms as $content)
                      {!! $content->description !!}
                  @endforeach
              @endif
            </div>
            <div class="col-xl-6">
              @if(\Session::has('alert-failed'))
                              <div class="alert alert-failed">
                                  <div>{{Session::get('alert-failed')}}</div>
                              </div>
                          @endif
                          @if(\Session::has('alert-success'))
                              <div class="alert alert-success">
                                  <div>{{Session::get('alert-success')}}</div>
                              </div>
                          @endif
              <form class="form form--contact-us" name="contact-us" action="{{route('contact-create')}}" method="POST" data-validation novalidate id="form">
                {{ csrf_field() }}

                <h2 class="title-1">ENQUIRY FORM</h2>
                <P>*Indicates field is required</P>

                <div class="form-group">
                  <label class="ct-label" for="enquiry">Type of Enquiry*</label>
                  <div class="ct-select">
                    <select class="custom-select__select" data-rule-required="required" data-msg-required="Required." name="type" id="type" required>
                      <option value="">Select</option>
                      <option value="General Enquiries and Feedback">General Enquiries and Feedback</option>
                      <option value="Atrium and Advertising Spaces">Atrium and Advertising Spaces</option>
                      <option value="Leasing (Permanent Shop Spaces)">Leasing (Permanent Shop Spaces)</option>
                      <option value="U-POPP Loyalty Program">U-POPP Loyalty Program</option>
                    </select><span class="ct-select-icons"><i class="ico ico-arrow-top"></i><i class="ico ico-arrow-bottom"></i></span>
                  </div>
                </div>
                <div class="d-flex d-flex--customize-pd">
                  <div class="flex-shrink-1">
                    <div class="form-group">
                      <label class="ct-label" for="salutation">Salutation*</label>
                      <div class="ct-select">
                        <select class="custom-select__select" data-rule-required="required" data-msg-required="Required." name="salutation" id="salutation" required>
                          <option value="">Select</option>
                          <option value="Mr">Mr</option>
                          <option value="Miss">Miss</option>
                          <option value="Mrs">Mrs</option>
                        </select><span class="ct-select-icons"><i class="ico ico-arrow-top"></i><i class="ico ico-arrow-bottom"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="flex-grow-1">
                    <div class="form-group">
                      <label class="ct-label" for="name">Name*</label>
                      <div class="ct-input">
                        <input type="text" name="name" id="name" data-rule-required="{&quot;rule&quot;: [&quot;required&quot;, &quot;name&quot;], &quot;msg&quot;: [&quot;Please enter a name.&quot;, &quot;Please enter alphabetical characters only.&quot;]}">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="ct-label" for="contact-number">Contact Number*</label>
                  <div class="ct-input">
                    <input type="text" name="contact_numb" id="contact_numb" data-rule-required="{&quot;rule&quot;: [&quot;required&quot;, &quot;number&quot;], &quot;msg&quot;: [&quot;Please enter contact number.&quot;, &quot;Please enter numeric characters only.&quot;]}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="ct-label" for="email">Email Address*</label>
                  <div class="ct-input">
                    <input type="text" name="email" id="email" data-rule-required="{&quot;rule&quot;: [&quot;required&quot;, &quot;email&quot;], &quot;msg&quot;: [&quot;Please enter email address.&quot;, &quot;This is not a valid email.&quot;]}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="ct-label" for="enquiry-msg">Enquiry Message*</label>
                  <div class="ct-input">
                    <textarea data-rule-required="required" data-msg-required="Please enter enquiry message." name="message" id="message" rows="4" maxlength="1000"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="ct-checkbox">
                    <input type="checkbox" data-rule-required="required" data-msg-required="Please indicate that you accept the terms and conditions." name="terms-and-conditions" id="terms-and-conditions">
                    <label for="terms-and-conditions">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci culpa dolores explicabo facilis nihil officia quam quod rerum tempora vero? Adipisci commodi cum eos modi nostrum porro reprehenderit tenetur? Fugiat.</label>
                  </div>
                </div>
                <div class="form-group">

                  <div class="g-recaptcha" data-msg-required="Please complete captcha" data-sitekey="6LclL7cZAAAAAHzlrJsVEsEZ2wjCXTSINJK7r5_p" data-callback="correctCaptcha"></div>
                  @if ($errors->has('g-recaptcha-response'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{$errors -> first ('g-recaptcha-response')}}</strong>
                    </span>
                  @endif
                  <!-- "This recaptcha key is generated for UAT purpose only. For live implementation, a new recaptcha key should be generated." -->
                </div>
                <div class="btn-wrapper--center">
                  <button class="solid-red-btn" type="submit" id="submit" value="save">SEND</button>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
