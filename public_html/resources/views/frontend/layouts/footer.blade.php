<!-- Footer Global Component HTML here-->
<footer class="footer">
      <div class="container">
        <div class="footer__social social"><span class="social__title text-uppercase">Follow us on</span>
          <div class="social__link"><a class="social__link--fb" href="{{ url ('https://www.facebook.com/UnitedSquare/')}}" title="Facebook" rel="noopener noreferrer" target="_blank"><i class="ico ico-facebook"></i></a><a class="social__link--insta" href="{{ url ('https://www.instagram.com/unitedsquaremall/')}}" title="Instagram" rel="noopener noreferrer" target="_blank"><i class="ico ico-instagram"></i></a>
          </div>
        </div>
        <ul class="footer__list-link list-link d-flex">
          <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{route ('happening')}}" title="HAPPENINGS">HAPPENINGS</a>
          </li>
          <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{route ('promotion')}}" title="PROMOTIONS">PROMOTIONS</a>
          </li>
          <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{route('store')}}" title="STORES">STORES</a>
          </li>
          @if ($cms != "")
            @foreach ($cms as $content)
            <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{route ('cms', $content->slug)}}" title="{{$content->title}}" style="text-transform:uppercase;">{{$content->title}}</a>
            </li>
            @endforeach
          @endif
          <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{ url ('https://www.uol.com.sg/data-protection-policy/')}}" title="DATA PROTECTION POLICY" rel="noopener noreferrer" target="_blank">DATA PROTECTION POLICY</a>
          </li>
          <li class="list-link__item text-lg-center"><a class="d-inline-block" href="{{route ('sitemap')}}" title="SITEMAP">SITEMAP</a>
          </li>
        </ul>
        <p class="copyright">COPYRIGHT © 2020 UNITED SQUARE. ALL RIGHTS RESERVED.</p>

        <div class="associate">
            <div class="footer-logo-wrapper">
            	<div class="footer-logo-container">
                        <span class="associate__link"><img class="lazyload" alt="UOL Malls logo" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('images/logos/uol_logo.png')}}"></span>
                </div>
                <div class="footer-logo-container">
                        <div class="footer-logo-inline">
                        <div class="footer-logo-inline"><a class="associate__link" href="{{ url ('https://www.kinex.com.sg/')}}" title="KINEX" rel="noopener noreferrer" target="_blank"><img class="lazyload" alt="KINEX" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('images/logos/kinex_logo.png')}}" style="width:90px"></a></div>
                        <div class="footer-logo-inline"><a class="associate__link" href="{{ url ('https://www.velocitynovena.com/')}}" title="VELOCITY" rel="noopener noreferrer" target="_blank"><img class="lazyload" alt="VELOCITY" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('images/logos/velocity_logo.png')}}"></a></div>
                        <div class="footer-logo-inline"><a class="associate__link" href="{{ url ('https://www.upopp.com.sg/')}}" title="UPOPP" rel="noopener noreferrer" target="_blank"><img class="lazyload" alt="UPOPP" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('images/logos/upopp_logo.png')}}"></a></div>
                        </div>
              	</div>
        	</div>
        </div>



      </div>
    </footer>
    <!-- Footer Global Component HTML here-->
    <div class="global-components-group">
      <!-- Back to top Global Component HTML here-->
      <div class="back-to-top" data-back-to-top>
        <div class="back-to-top__wrapper">
          <div class="back-to-top__icon"><i class="ico ico-arrow-up"></i></div>
          <div class="back-to-top__text">
            <p class="text-uppercase">back to top</p>
          </div>
        </div>
      </div>
      <!-- Back to top Global Component HTML here-->
      <!-- Cookies Global Component HTML here-->
      <div class="cookie d-none" data-united-square-cookie data-cookie-name="isAgreed">
        <button class="btn btn-close" data-cookie-close><i class="ico ico-close"></i></button>
        <div class="container">
          <p>
            This site uses cookies. By continuing to browse through this site, you are consenting to our use of cookies. View our&nbsp;<a href="{{ url ('https://www.uol.com.sg/data-protection-policy/')}}" title="Data Protection Policy" rel="noopener noreferrer" target="_blank">Data Protection Policy</a>&nbsp;to learn more.
          </p>
        </div>
      </div>
      <!-- Cookies Global Component HTML here-->
      <!-- Initial Load Screen Global Component HTML here-->

      @if($popup)
      <div class="initial-loads-screen" data-initial-screen data-cookie-name="isModalOpened">
        <div class="modal" id="initalLoadScreen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <button class="btn" type="button" data-dismiss="modal" aria-label="Close"><i class="ico ico-close"></i></button>
              <div class="modal-body">
                <div class="hero-banner slider">
                  <div class="container">
                    <div class="slider slick-slider" data-slider>
                        @foreach ($popup as $pop)
                          @if($pop->link != "")
                            <a href="{{asset('/storage/banners/links/'.$pop->link)}}" target="_blank">
                              <img class="lazyload" alt="United Square Banner" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$pop->image)}}"></a>
                          @else
                            <img class="lazyload" alt="{{$pop->name}}" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$pop->image)}}">
                          @endif
                        @endforeach
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      <!-- Initial Load Screen Global Component HTML here-->
    </div>
    <!-- main:js -->
    <script src="{{ asset ('scripts/main.min.js')}}"></script><!-- endinject -->
    <script src="{{ url ('https://www.google.com/recaptcha/api.js')}}" async="" defer=""></script>
    @yield("js")

      </body>
