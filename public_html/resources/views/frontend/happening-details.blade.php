@extends("frontend.layouts.app")
@section("title")
    {{$HappeningDetails -> name}} | United Square
@endsection

@section("content")

<div class="breadcrumb">
  <div class="container">
    <ul class="breadcrumb__list">
      <li class="breadcrumb__item"><a class="breadcrumb__link" href="{{url('/')}}" title="Home"><i class="ico ico-home"></i></a>
      </li>
      <li class="breadcrumb__item"><a class="breadcrumb__link" href="{{route('happening')}}" title="HAPPENINGS">HAPPENINGS</a>
      </li>
      <li class="breadcrumb__item">{{ $HappeningDetails -> name}}</li>
    </ul>
  </div>
</div>
<div class="article-detail">
<div class="container">
<div class="row">
  <div class="col-lg-6">
    <div class="happenings-slider">
      <div class="slider slick-slider" data-slider>
        @if ($HappeningDetails->image != ""  )
          @if (strpos($HappeningDetails->image, '|') !== false)
            @foreach (explode ("| ", $HappeningDetails->image) as $pict)
                <img class="lazyload" alt="" src="{{asset ('images/transparent.png')}}" data-src="{{asset ('storage/happening/images/'.$pict)}}">
            @endforeach
          @else
            <img class="lazyload" alt="" src="{{asset ('images/transparent.png')}}" data-src="{{asset ('storage/happening/images/'.$HappeningDetails->image)}}">
          @endif
        @endif
      </div>
    </div><br>
    <!-- <div class="happenings-gallery">
      <div class="happenings-gallery__title">Event Gallery</div>
      <div class="slider-wrapper">
        <div class="slider slider--gallery slick-slider" data-slider-gallery>
          @if ($HappeningDetails->image != ""  )
            @if (strpos($HappeningDetails->image, '|') !== false)
              @foreach (explode ("| ", $HappeningDetails->image) as $pict)
                <div class="slider-item"><a href="{{asset ('storage/happening/images/'.$pict)}}" title="Happenings Gallery" data-fancybox="images">
                  <img alt="Happenings Slider" src="{{asset ('storage/happening/images/'.$pict)}}"></a>
                </div>
              @endforeach
            @else
            <div class="slider-item"><a href="{{asset ('storage/happening/images/'.$HappeningDetails->image)}}" title="Happenings Gallery" data-fancybox="images">
              <img alt="Happenings Slider" src="{{asset ('storage/happening/images/'.$HappeningDetails->image)}}"></a>
            </div>
            @endif
        @endif
        </div>
      </div>
    </div> -->
  </div>
  <div class="col-lg-6">
    <div class="article-detail-content">
      <h1 class="article-detail__title">{{ $HappeningDetails -> name}}</h1>
      @if($HappeningDetails->start_date || $HappeningDetails->end_date)
      <span class="article__date">
        <!-- @if(date('j F Y') >= $HappeningDetails->start_date || date('j F Y') <= $HappeningDetails->end_date) -->

          Now
        <!-- @else
          {{ \Carbon\Carbon::parse($HappeningDetails->start_date)->format('j F Y')}}
        @endif -->
        - {{ \Carbon\Carbon::parse($HappeningDetails->end_date)->format('j F Y')}}</span>
      @endif
      {!! $HappeningDetails -> description !!}
    </div>
    @if($HappeningDetails -> registration == 1)
    <div class="btn-wrapper--center"><a class="solid-red-btn" href="#" title="REGISTER NOW!">REGISTER NOW!</a>
    </div>
    @endif
    <!-- <div class="article-img-wrapper article-img-wrapper--normal"><img class="lazyload" alt="Article Detail Images" src="{{asset ('assets/images/transparent.png')}}" data-src="{{asset ('assets/images/transparent.png')}}"> -->
    <!-- </div> -->
    <br>
    @if($HappeningDetails->term_desc)
    <div class="accordion" id="accordion">
      <div id="headingOne">
        <button class="accordion__control collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Terms & Conditions</button>
      </div>
      <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="accordion-body">
          {!! $HappeningDetails->term_desc !!}
        </div>
      </div>
    </div>
    @endif
  </div>
</div>
</div>
</div>

@endsection
