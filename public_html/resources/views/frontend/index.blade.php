@extends("frontend.layouts.app")

@section("title")
    Home | United Square
@endsection

@section("content")

    <!-- Header Load Screen Global Component HTML here-->
      @if ($count > 1)
      <div class="hero-banner slider">
        <div class="container">
          <div class="slider slick-slider" data-slider>

            @foreach ($HomeBanner as $banner)
              @if($banner->link != "")
                <a href="{{asset('/storage/banners/links/'.$banner->link)}}" target="_blank"><img class="lazyload" alt="United Square Banner" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$banner->image)}}"></a>
              @else
                <img class="lazyload" alt="United Square Banner" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$banner->image)}}">
              @endif
            @endforeach

            </div>
        </div>
      </div>

      @else
      <div class="banner">
        @foreach ($HomeBanner as $banner)
        <div class="container">
        @if($banner->link != "")
          <a href="{{asset('/storage/banners/links/'.$banner->link)}}" target="_blank"><img class="lazyload" alt="{{$banner->name}}" src="{{asset('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$banner -> image)}}"></a>
        @else
          <img class="lazyload" alt="{{$banner->name}}" src="{{asset('images/transparent.png')}}" data-src="{{ asset ('storage/banners/'.$banner -> image)}}">
        @endif
      </div>
        @endforeach
      </div>
      @endif
      <div class="section happenings">
        <div class="container">
          <div class="happenings-list">
            <div class="happening-slider-wrap happenings__item">
              <div class="title-section title-section--dash-blue">
                <div class="title-section__text">HAPPENINGS</div>
              </div>

              <div class="slider slick-slider slider-for" data-slider data-slider-options="{ &quot;autoplaySpeed&quot;: 12000 }">
                @if($HappeningCurrent != "")
                  @foreach($HappeningCurrent as $happen)
                    @if ($happen->image_details != "")
                      <a href="{{route('happening-details', $happen->id)}}" title="{{$happen->name}}">
                        <img class="lazyload" alt="{{$happen->name}}" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/happening/images/'.$happen->image_details) }}">
                      </a>
                    @endif
                  @endforeach
                @endif
              </div>

              <div class="slider slick-slider slider-nav">
                <div class="view-more">
                  <a href="{{route('happening')}}" title="view more"><i class="ico ico-arrow-right1"></i><span>View more</span></a>
                </div>
              </div>

            </div>
            <div class="promotions-slider-wrap happenings__item">
              <div class="title-section title-section--dash-green">
                <div class="title-section__text">PROMOTIONS</div>
              </div>
              <div class="slider slick-slider slider-for-1" data-slider data-slider-options="{ &quot;autoplaySpeed&quot;: 12000 }">
                @if($Promotion != "")
                  @foreach($Promotion as $promo)
                    @if ($promo->promo_image != "")
                      <a href="{{route ('promotion-details', $promo->id)}}" title="{{$promo->name}}">
                        <img class="lazyload" alt="{{$promo->name}}" src="{{ asset ('images/transparent.png')}}" data-src="{{ asset ('storage/promotion/images/'.explode('|', $promo->promo_image)[0]) }}">
                      </a>
                    @endif
                  @endforeach
                @endif
              </div>
              <div class="slider slick-slider slider-nav-1">
                <div class="view-more"><a href="{{route('promotion')}}" title="view more"><i class="ico ico-arrow-right1"></i><span>View more</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section">
        <div class="container connect insta-list">
          <div class="title-section title-section--dash-yellow mb-0">
            <div class="title-section__text">CONNECT WITH US!</div>
          </div>
          <div class="connect__list my-2">
            @foreach ($insta as $item)
            <a class="insta connect__item" href="{{url('https://www.instagram.com/unitedsquaremall/')}}" title="Instagram" target="_blank">
              <img class="lazyload insta__img" alt="Instagram" src="{{ asset ('images/transparent.png')}}" data-src="{{ url ($item)}}">
              <i class="ico ico-instagram"></i>
            </a>
            @endforeach

          </div>
        </div>
      </div>

@endsection
