<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner\Banner;
use App\Models\Cms\Cms;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

class CmsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index ($slug)
    {
      if($slug){
        $content_cms = Cms::where('slug', $slug)->orderBy('id', 'ASC')->get();

          if(count($content_cms)>0){
            foreach ($content_cms as $content) {
                $page = $content->id;
              }
            }else{
              return view('frontend/cms/page-not-found');
            }

          if($page){
            $CmsBanner = Banner::where([['status', 1],['page_id', $page],['type', 2],['start_datetime', '<=', Carbon::now()],
                                         ['end_datetime', '>=', Carbon::now()]])
                                  ->orderBy('ordering', 'ASC')
                                  ->get();
          }


        if($slug == "contact-us"){
          return view('frontend/cms/contact-us',['body_class'=>$slug.'-page'], compact('content_cms','CmsBanner'));
        }else{
          return view('frontend/cms/default-cms',['body_class'=>$slug.'-page'], compact('content_cms','CmsBanner'));
        }
      }
    }
}
